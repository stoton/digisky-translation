package eu.digisky.translation;

interface TranslationCache {

    void cacheByLanguage(Language language);

    Translation getTranslationByKey(TranslationKey translationKey, Language language);

    void invalidate();

}
