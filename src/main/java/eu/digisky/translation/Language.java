package eu.digisky.translation;

public enum Language {
    ENGLISH("ENGLISH"), FRENCH("FRENCH"), POLISH("POLISH"), DUTCH("DUTCH"), GERMAN("GERMAN"),
    ITALIAN("ITALIAN"), SPANISH("SPANISH");

    private final String value;

    Language(String value) {
        this.value = value;
    }

    static Language from(String language) {
        if ("ENGLISH".equalsIgnoreCase(language)) {
            return ENGLISH;
        } else if ("FRENCH".equalsIgnoreCase(language)) {
            return FRENCH;
        } else if ("POLISH".equalsIgnoreCase(language)) {
            return POLISH;
        } else if ("DUTCH".equalsIgnoreCase(language)) {
            return DUTCH;
        } else if ("GERMAN".equalsIgnoreCase(language)) {
            return GERMAN;
        } else if ("ITALIAN".equalsIgnoreCase(language)) {
            return ITALIAN;
        } else if ("SPANISH".equalsIgnoreCase(language)) {
            return SPANISH;
        }
        throw new IllegalArgumentException("Unknown name of language: " + language);
    }

    @Override
    public String toString() {
        return value;
    }
}
