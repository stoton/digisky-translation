package eu.digisky.translation;

import java.util.List;
import java.util.Optional;

interface TranslationRepository {

    Optional<Translation> fetchByTranslationKey(TranslationKey translationKey, Language language);

    List<Translation> fetchByLanguage(Language language);

}
