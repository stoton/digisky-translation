package eu.digisky.translation;

import lombok.Data;

@Data
class TranslationKey {
    private String context;
    private String phrase;
    private String id;

    public TranslationKey(String context, String phrase, String id) {
        this.context = context;
        this.phrase = phrase;
        this.id = id;
    }
}
