package eu.digisky.translation;

import java.util.HashMap;
import java.util.Map;

class TranslationCacheImpl implements TranslationCache {
    private final TranslationRepository translationRepository;
    private final Map<String, Translation> translations;

    public TranslationCacheImpl(TranslationRepository translationRepository) {
        this.translationRepository = translationRepository;
        translations = new HashMap<>();
    }

    @Override
    public void cacheByLanguage(Language language) {
        translationRepository.fetchByLanguage(language)
                .forEach(translation -> {
                    TranslationKey translationKey = new TranslationKey(
                            translation.getContext(),
                            translation.getPhrase(),
                            translation.getIdOfTranslatedObject()
                    );
                    translations.put(createKeyFromTranslationKeyAndLanguage(translationKey, language), translation);
                });
    }

    @Override
    public Translation getTranslationByKey(TranslationKey translationKey, Language language) {
        String key = createKeyFromTranslationKeyAndLanguage(translationKey, language);

        if (translations.containsKey(key)) {
            return translations.get(key);
        }

        Translation translation = translationRepository
                .fetchByTranslationKey(translationKey, language)
                .orElse(translationWhenNotFoundInDatabase(translationKey, language));

        translations.put(key, translation);
        return translations.get(key);
    }

    @Override
    public void invalidate() {
        translations.clear();
    }

    private String createKeyFromTranslationKeyAndLanguage(TranslationKey translationKey, Language language) {
        return translationKey.getContext()
                + "_" + translationKey.getPhrase()
                + "_" + translationKey.getId()
                + "_" + language.toString();
    }

    private Translation translationWhenNotFoundInDatabase(TranslationKey translationKey, Language language) {
        Translation translation = new Translation();
        translation.setContext(translationKey.getContext());
        translation.setPhrase(translationKey.getPhrase());
        translation.setIdOfTranslatedObject(translationKey.getId());
        translation.setTranslation(translationKey.getPhrase());
        translation.setLanguage(language);

        return translation;
    }
}
