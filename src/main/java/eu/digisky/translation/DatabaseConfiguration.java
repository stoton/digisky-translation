package eu.digisky.translation;

import lombok.Data;

@Data
public class DatabaseConfiguration {
    private String url;
    private String userName;
    private String password;
}
