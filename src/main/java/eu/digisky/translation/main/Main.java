package eu.digisky.translation.main;

import eu.digisky.translation.DatabaseConfiguration;
import eu.digisky.translation.Language;
import eu.digisky.translation.TranslationFacade;

public class Main {
    public static void main(String[] args) {
        DatabaseConfiguration databaseConfiguration = new DatabaseConfiguration();
        databaseConfiguration.setUrl("jdbc:postgresql://localhost:5432/postgres?serverTimezone=UTC");
        databaseConfiguration.setUserName("postgres");
        databaseConfiguration.setPassword("postgres");

        TranslationFacade translationFacade = new TranslationFacade(databaseConfiguration);

            translationFacade.cacheByLanguage(Language.POLISH);

        String translatedOwner = translationFacade
                .translate("ARTICLE", "szczotka", "1", Language.ENGLISH);

        System.out.println(translatedOwner);

//        String translatedCity = translationFacade
//                .translate("attributes", "city", 1L, Language.FRENCH);

//        System.out.println(translatedCity);


        System.exit(0);
    }
}