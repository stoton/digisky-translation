package eu.digisky.translation;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

class TranslationRepositoryImpl implements TranslationRepository {
    private final static String SELECT_ALL_BY_LANGUAGE = "SELECT * FROM TRANSLATIONS WHERE LANGUAGE = UPPER(#PARAM#)";
    private final static String SELECT_BY_TRANSLATION_KEY = "SELECT * FROM TRANSLATIONS " +
            "WHERE ID_OF_TRANSLATED_OBJECT = #PARAM# " +
            "AND CONTEXT = #PARAM# " +
            "AND PHRASE = #PARAM# " +
            "AND UPPER(LANGUAGE) LIKE UPPER(#PARAM#) ";
    private final static String TRANSLATIONS_ID = "id";
    private final static String TRANSLATIONS_CONTEXT = "context";
    private final static String TRANSLATIONS_PHRASE = "phrase";
    private final static String TRANSLATIONS_ID_OF_TRANSLATED_OBJECT = "id_of_translated_object";
    private final static String TRANSLATIONS_TRANSLATION = "translation";
    private final static String TRANSLATIONS_LANGUAGE = "language";

    private final DatabaseConfiguration dbConf;

    public TranslationRepositoryImpl(DatabaseConfiguration dbConf) {
        this.dbConf = dbConf;
    }

    @Override
    public Optional<Translation> fetchByTranslationKey(TranslationKey translationKey, Language language) {
        List<Translation> translations = selectTranslationBySql(fillSqlWithParam(SELECT_BY_TRANSLATION_KEY,
                String.valueOf(translationKey.getId()), translationKey.getContext(),
                translationKey.getPhrase(), language.toString()));

        if (!hasUniqueResult(translations)) {
            throw new RuntimeException("Not unique result for translation key: "
                    + translationKey.getContext()
                    + "_" + translationKey.getPhrase()
                    + "_" + translationKey.getId()
            );
        }
        return translations.isEmpty() ? Optional.empty() : Optional.of(translations.get(0));
    }

    @Override
    public List<Translation> fetchByLanguage(Language language) {
        return selectTranslationBySql(fillSqlWithParam(SELECT_ALL_BY_LANGUAGE, language.toString()));
    }

    private List<Translation> selectTranslationBySql(String sql) {
        List<Translation> translations = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(
                dbConf.getUrl(), dbConf.getUserName(), dbConf.getPassword());
             Statement statement = connection.createStatement()
        ) {
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Translation translation = new Translation();

                translation.setId(resultSet.getLong(TRANSLATIONS_ID));
                translation.setContext(resultSet.getString(TRANSLATIONS_CONTEXT));
                translation.setPhrase(resultSet.getString(TRANSLATIONS_PHRASE));
                translation.setIdOfTranslatedObject(resultSet.getString(TRANSLATIONS_ID_OF_TRANSLATED_OBJECT));
                translation.setTranslation(resultSet.getString(TRANSLATIONS_TRANSLATION));
                translation.setLanguage(Language.from(resultSet.getString(TRANSLATIONS_LANGUAGE)));
                translations.add(translation);
            }
            resultSet.close();
        } catch (Exception exception) {
            throw new RuntimeException("problems with database.. " + exception.getMessage());
        }
        return translations;
    }

    private String fillSqlWithParam(String sql, String... params) {
        for (String param : params) {
            sql = sql.replaceFirst("#PARAM#", "'" + param + "'");
        }
        return sql;
    }

    private boolean hasUniqueResult(List<Translation> translations) {
        return translations.size() < 2;
    }
}