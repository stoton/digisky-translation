package eu.digisky.translation;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
class Translation {
    private Long id;
    private String idOfTranslatedObject;
    private String context;
    private String phrase;
    private String translation;
    private Language language;

    public Translation() {
    }

    public Translation(String idOfTranslatedObject, String context, String phrase, String translation, Language language) {
        this.idOfTranslatedObject = idOfTranslatedObject;
        this.context = context;
        this.phrase = phrase;
        this.translation = translation;
        this.language = language;
    }
}
