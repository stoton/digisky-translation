package eu.digisky.translation;

public class TranslationFacade {
    private final TranslationCache translationCache;

    public TranslationFacade(DatabaseConfiguration databaseConfiguration) {
        TranslationRepository translationRepository = new TranslationRepositoryImpl(databaseConfiguration);
        translationCache = new TranslationCacheImpl(translationRepository);
    }

    public String translate(String context, String phrase, String idOfTranslatedObject, Language language) {
        TranslationKey translationKey = new TranslationKey(context, phrase, idOfTranslatedObject);
        return translationCache.getTranslationByKey(translationKey, language).getTranslation();
    }

    public void invalidate() {
        translationCache.invalidate();
    }

    public void cacheByLanguage(Language language) {
        translationCache.cacheByLanguage(language);
    }
}
