package eu.digisky.translation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TranslationCacheImplTest {
    @Mock
    private TranslationRepository translationRepository;
    private TranslationCache translationCache;

    @BeforeEach
    void init() {
        translationCache = new TranslationCacheImpl(translationRepository);
    }

    @Test
    void shouldCacheByLanguage() {
        when(translationRepository.fetchByLanguage(Language.ENGLISH)).thenReturn(List.of(
                new Translation(1L, "1", "attributes", "owner", "owner", Language.ENGLISH)
        ));

        translationCache.cacheByLanguage(Language.ENGLISH);
        translationCache.getTranslationByKey(new TranslationKey("attributes", "owner", "1"), Language.ENGLISH);

        verify(translationRepository, times(0))
                .fetchByTranslationKey(new TranslationKey("attributes", "owner", "1"), Language.ENGLISH);
    }

    @Test
    void shouldGetTranslationByKey() {
        Translation expected = new Translation(1L, "1", "attributes", "city", "ville", Language.FRENCH);

        TranslationKey translationKey = new TranslationKey("attributes", "city", "1");

        when(translationRepository.fetchByTranslationKey(translationKey, Language.FRENCH)).thenReturn(
                Optional.of(new Translation(1L, "1", "attributes", "city", "ville", Language.FRENCH))
        );

        Translation actual = translationCache.getTranslationByKey(translationKey, Language.FRENCH);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void shouldReturnSameTranslationAsInputPhraseWhenTranslationNotFound() {
        Translation expected = new Translation(null, "1", "attributes", "owner", "owner", Language.ENGLISH);

        TranslationKey translationKey = new TranslationKey("attributes", "owner", "1");
        when(translationRepository.fetchByTranslationKey(translationKey, Language.ENGLISH)).thenReturn(Optional.empty());

        Translation actual = translationCache.getTranslationByKey(translationKey, Language.ENGLISH);

        assertThat(actual).isEqualTo(expected);
    }
}